import React, { useState, useEffect } from 'react';
import axios from 'axios';

const POSTURL = `http://elsevier3.conygre.com:8080/signup`;


const SignUpForm = () => {


    const [Title, setTitle] = useState('Mrs');
    const [onlineStatus, setOnlineStatus] = useState(false);
    const [Firstname, setFirstName] = useState([]);
    const [Lastname, setLastName] = useState([]);
    const [Email, setEmail] = useState([]);
    const [DOB, setDob] = useState("2019-09-06");
    const [PhoneNumber, setPhoneNo] = useState([]);
    const [Gender, setGender] = useState([]);

    const sendData = async (e) => {

        let fullData = {}
        fullData['Title'] = Title;
        fullData['Firstname'] = Firstname;
        fullData['Lastname'] = Lastname;
        fullData['Email'] = Email;
        fullData['DOB'] = DOB;
        fullData['PhoneNumber'] = PhoneNumber;
        fullData['Gender'] = Gender;
        //fullData['_id'] = Title + ' ' + Firstname + ' ' + Lastname;

        console.log(fullData)
        await axios({
            method: 'post',
            url: POSTURL,
            data: fullData,
            headers: { 'Content-Type': 'application/json' },
            validateStatus: (status) => {
                console.log(status)
                return true; // I'm always returning true, you may want to do it depending on the status received
            },
        }).then(response => {
            console.log("ahhhhhhhhhhhhh")
            console.log(response)
            // this is now called!

        }).catch(error => {
            console.log(error)
        })
        setOnlineStatus(true);

        // try {
        //     //  axios.post(POSTURL, fullData);

        //     await axios({
        //         method: 'post',
        //         url: POSTURL,
        //         data: fullData,
        //         validateStatus: (status) => {
        //             return true; // I'm always returning true, you may want to do it depending on the status received
        //         },
        //     }).catch(error => {

        //     }).then(response => {
        //         console.log(wooo)
        //         // this is now called!
        //     });
        //     setOnlineStatus(true);
        // } catch (e) {
        //     console.log(e)
        //     setOnlineStatus(false);
        // }
    };

    return (
        <div class='container center'>
            <form onSubmit={sendData}>
                <div class="form-group row">
                    <label for='title' class='col-sm-2 col-form-label'>Title*</label>
                    <div class="col-md-6">
                        <select id='title' class='vrounded' required onChange={event => setTitle(event.target.value)}>
                            <option value='Mrs'>Mrs</option>
                            <option value='Mr'>Mr</option>
                            <option value='Miss'>Miss</option>
                            <option value='Ms'>Ms</option>
                            <option value='Dr'>Dr</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="firstname" class='col-sm-2 col-form-label'>First Name*</label>
                    <div class="col-md-6">
                        <input type='text' id="firstname" class='vrounded' onChange={event => setFirstName(event.target.value)} required />
                    </div>
                </div>
                <div class="form-group row">
                    <label for="lastname" class='col-sm-2 col-form-label'>Last Name*</label>
                    <div class="col-md-6">
                        <input type='text' id="lastname" class='vrounded' onChange={event => setLastName(event.target.value)} required />
                    </div>
                </div>
                <div class="form-group row">
                    <label for="email" class='col-sm-2 col-form-label'>Email*</label>
                    <div class="col-md-6">
                        <input type='email' id="email" class='vrounded' onChange={event => setEmail(event.target.value)} required />
                    </div>
                </div>
                <div class="form-group row">
                    <label for="date" class='col-sm-2 col-form-label'>Date of Birth</label>
                    <div class="col-md-6">
                        <input type='date' id="date" class='vrounded' onChange={event => setDob(event.target.value)} />
                    </div>
                </div>
                <div class="form-group row">
                    <label for="phone number" class='col-sm-2 col-form-label'>Phone Number</label>
                    <div class="col-md-6">
                        <input type='text' id="phone number" class='vrounded' onChange={event => setPhoneNo(event.target.value)} />
                    </div>
                </div>
                <div class="form-group row">
                    <label for="check" class='col-sm-2 col-form-label'></label>
                    <div class="col-md-6">
                        <input type='radio' name='gender' id="check" onChange={event => setGender("Male")} /> Male <br />
                        <input type='radio' name='gender' id="check" onChange={event => setGender("Female")} /> Female <br />
                        <input type='submit' value="Sign me up!" class='bg-primary text-white smallwidth' onSubmit={sendData} />
                    </div>
                </div>
                <div>

                </div>
            </form>
        </div>
    );
}

export default SignUpForm;