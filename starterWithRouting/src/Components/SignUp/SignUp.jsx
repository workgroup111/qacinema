import React from 'react';
import "bootstrap/dist/css/bootstrap.min.css";
import SignUpForm from './SignUpForm';


const SignUp = () => {

    // constructor(props) {
    //     super(props);
    //     this.state = { value: '' };

    //     this.handleChange = this.handleChange.bind(this);
    //     this.handleSubmit = this.handleSubmit.bind(this);
    // };

    // handleChange(event) {
    //     this.setState({ value: event.target.value });
    // };

    // handleSubmit(event) {
    //     alert('A name was submitted: ' + this.state.value);
    //     event.preventDefault();
    // };

    return (
        <SignUpForm />
    );
};

export default SignUp;