import React from 'react';
import popcorn_img from '../images/popcorn.jpeg';
import NavBar from './NavBar';

const Header = () => {
    return (
        <>
            <div className="container sticky-top" id="qa-navbar">
                <NavBar />
            </div>
            <div align="center">
                <img id='bannerimg' src={popcorn_img} width="100%"/>
            </div>
        </>
    );
}

export default Header;