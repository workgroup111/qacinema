import React from 'react';
import "bootstrap/dist/css/bootstrap.min.css";


import Time from './Time.jsx'

const OpeningHours = props => {

    let openingTime = [];
    console.log(props.openingTimes)
    if (typeof props.openingTimes[0] !== "string") {
        openingTime = props.openingTimes.map(currentTime => (
        <Time
            time={currentTime}
            key={currentTime._id} />
        ));
    } else if (props.loading) {
    openingTime.push(
      <tr key="loading">
        <td colSpan="3">Please wait - retrieving the times</td>
      </tr>
    );
    } else {
        openingTime.push(
        <tr key="error">
            <td colSpan="3">There has been an error retrieving the times</td>
        </tr>
        );
    }
  
    return (
        <>
            <div className='container'>
                <h2>Opening Times</h2>
                <table className="table table-striped" width='50%'>
                    <thead>
                        <tr>
                            <th>Day</th>
                            <th>Opening </th>
                            <th>Closing</th>
                        </tr>
                    </thead>
                    <tbody>
                        {openingTime}
                    </tbody>
                </table>
            </div>
         </>
    )



    // return (
    //     <div className='Container'>
    //         <h2> Opening Hours </h2>
    //         <table className="table table-striped" width='50%'>
    //             <tbody>
    //                 <tr>
    //                     <td>Monday - Thursday</td>
    //                     <td>16:30-21:00</td>
    //                 </tr>
    //             <tr>
    //                 <td>Friday - Saturday</td>
    //                 <td>16:30-22:00</td>
    //             </tr>
    //             <tr>
    //                 <td>Sunday</td>
    //                 <td>16:00-21:30</td>
    //             </tr>
    //             </tbody>    
    //         </table>
    //     </div>

    // )

}

export default OpeningHours;