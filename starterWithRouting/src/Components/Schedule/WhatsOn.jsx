import React from 'react';

import Film from './Film.jsx'

const WhatsOn = props => {
  let perfilm = [];
  console.log(props.filmdata)
  if (typeof props.filmdata[0] !== "string") {
    perfilm = props.filmdata.map(currentFilm => (
      <Film
        film={currentFilm}
        key={currentFilm._id} />
    ));
  } else if (props.loading) {
    perfilm.push(
      <tr key="loading">
        <td colSpan="3">Please wait - retrieving the films</td>
      </tr>
    );
  } else {
    perfilm.push(
      <tr key="error">
        <td colSpan="3">There has been an error retrieving the films</td>
      </tr>
    );
  }

    return (
        <>
            <div className='container'>
                <h2>Films Showing</h2>
                <table className="table table-striped" width='50%'>
                    <thead>
                        <tr>
                            <th>Movie Title</th>
                            <th>Showing Time</th>
                            <th>Poster</th>
                        </tr>
                    </thead>
                    <tbody>
                        {perfilm}
                    </tbody>
                </table>

      </div>
      {/* <table align="center" width='75%'>
            <t1> Opening Hours </t1>
            <tr>
                <td>Monday - Thursday</td>
                <td>16:30-21:00</td>
            </tr>
            <tr>
                <td>Friday - Saturday</td>
                <td>16:30-22:00</td>
            </tr>
            <tr>
                <td>Sunday</td>
                <td>16:00-21:30</td>
            </tr>
            </table> */}
    </>
  )
}

export default WhatsOn;