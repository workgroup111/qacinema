import React, { useState, useEffect } from 'react';
import axios from 'axios';
import "bootstrap/dist/css/bootstrap.min.css";
import OpeningHours from './OpeningHours';
import WhatsOn from './WhatsOn';

const ALLFILMSURL = `http://elsevier3.conygre.com:8080/allfilms`;
const ALLTIMESURL = `http://elsevier3.conygre.com:8080/openingTimes`;

const Schedule = () => {

    const [filmdata, setFilmdata] = useState([]);
    const [openingTimes, setopeningTimes] = useState([]);
    const [onlineStatus, setOnlineStatus] = useState(false);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        setTimeout(() => {
            if (typeof filmdata !== "string")
                getFilmdata();
            getopeningTimes();
            console.log("whaat")
        }, 50);
    }, []);

    const getFilmdata = async () => {
        setLoading(true);
        try {
            const res = await axios.get(ALLFILMSURL);
            const gotfilmdata = await res.data;
            console.log(gotfilmdata)
            setFilmdata(gotfilmdata);
            setLoading(false);
            setOnlineStatus(true);
        }
        catch (e) {
            setFilmdata(e.message);
            setLoading(false);
            setOnlineStatus(false);
        }
    };

    const getopeningTimes = async () => {
        setLoading(true);
        try {
            const res = await axios.get(ALLTIMESURL);
            const gotopeningTimes = await res.data;
            console.log(gotopeningTimes)
            setopeningTimes(gotopeningTimes);
            setLoading(false);
            setOnlineStatus(true);
        }
        catch (e) {
            setopeningTimes(e.message);
            setLoading(false);
            setOnlineStatus(false);
        }
    };


    return (
        <div class='container'>
            {/* Add data here then change file then should be fine :) */}
            <OpeningHours openingTimes={openingTimes} loading={loading} />
            {!onlineStatus && !loading ? (
                <h3>The data server may be offline</h3>
            ) : null}
            <WhatsOn filmdata={filmdata} loading={loading} />
        </div>
    );
}


export default Schedule;