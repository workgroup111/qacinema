import React from 'react';
// import film2Image from './film2Image.jpg';
// import film1Image from './film1Image.jpg';
// import film3Image from './film3Image.jpg';
import { FILMIMAGES } from './FILMIMAGES';

const Film = props => {
    // import filmImage from ('.' + props.film.img);


    return (
        <tr>
            <td>{props.film.title}</td>
            <td>{props.film.showingTimes}</td>
            <td>{props.film.poster} <img src={[FILMIMAGES[props.film.title].src]} alt="Poster will display here" width="112.5" height="150"></img></td>
            {/* <td><img src={film1Image} alt="Poster will display here" width="50" height="50"></img></td>
            <td><img src={film2Image} alt="Poster will display here" width="50" height="50"></img></td>
            <td><img src={film3Image} alt="Poster will display here" width="50" height="50"></img></td> */}
            {/* <td><img src="./film2Image.jpg"></img></td> */}
        </tr>
    );
};
export default Film;


