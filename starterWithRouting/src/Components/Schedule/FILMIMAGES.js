export const FILMIMAGES = {
    "Test Film 1":
        { src: `../images/film1Image.jpg`, alt: `Test Film 1` },
    "Test Film 2":
        { src: `../images/film2Image.jpg`, alt: `Test Film 2` },
    "Test Film 3":
        { src: `../images/film3Image.jpg`, alt: `Test Film 3` },
};