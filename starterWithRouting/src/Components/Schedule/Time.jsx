import React from 'react';
// import film2Image from './film2Image.jpg';
// import film1Image from './film1Image.jpg';
// import film3Image from './film3Image.jpg';

const Time = props => {


    return (
        <tr>
            <td>{props.time.day}</td>
            <td>{props.time.opening}</td>
            <td>{props.time.close}</td>
        </tr>
    );
};
export default Time;