import React from 'react';
import Paragraph from './Paragraph.jsx';
import ImageCards from './ImageCards.jsx';

const Home = () => {
    return (
        <div>
            <Paragraph />
            <br/><br/>
            <ImageCards />
            <Paragraph />
        </div>


    );
}

export default Home;