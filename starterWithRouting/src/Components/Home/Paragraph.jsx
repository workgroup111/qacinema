import React from 'react';

import { CONTENTARTICLETEXT } from './contentArticleText.js';

const Paragraph = () => {
    return (
        <p>{CONTENTARTICLETEXT}</p>
    );
}

export default Paragraph;