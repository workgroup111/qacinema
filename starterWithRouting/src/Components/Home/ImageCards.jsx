import React from 'react';
import card_img1 from '../images/InternationalHouse.jpeg';
import card_img2 from '../images/InternationalHouse2.jpeg';
import card_img3 from '../images/Locations.jpg';
import "bootstrap/dist/css/bootstrap.min.css";

const ImageCards = () => {
    return (
        <div className="container-fluid">
            <div className="row" align='center'>

                <div className="col-lg-2 col-md-2 col-xs-2 thumb no-gutters">
                    <a className="thumbnail" href="#">
                        <img class="img-responsive" src={card_img1} alt="" align='left' height="75%"></img>
                    </a>
                </div>
                <div class="clearfix visible-lg clearfix visible-md clearfix visible-sm"></div>
                <div className="col-lg-8 col-md-8 col-xs-8 thumb no-gutters">
                    <a className="thumbnail" href="#">
                        <img class="img-responsive" src={card_img3} alt="" align="middle" height="75%"></img>
                    </a>
                </div>
                <div class="clearfix visible-lg clearfix visible-md clearfix visible-sm"></div>
                <div className="col-lg-2 col-md-2 col-xs-2 thumb no-gutters">
                    <a className="thumbnail" href="#">
                        <img class="img-responsive" src={card_img2} alt="" align="right" height="75%"></img>
                    </a>
                </div>
                <div class="clearfix visible-lg clearfix visible-md clearfix visible-sm"></div>
            </div>
        </div>
    );
}

export default ImageCards;