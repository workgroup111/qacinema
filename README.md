This folder has the following contents:

/backend
This contains the docker-compose.yml file needed to run and actual Node/Mongo backend service using Docker.
Before using this file, ensure that you have mapped the volume to the location of the /data/db folder inside this folder to enable persistance of the data held in MongoDB.

/solution
This contains an example React application that has been created to fulfill the requirements of the Hackathon.
However, no testing is included here.

/starterAssets
If delegates are creating a new application then images, CSS and mock data can be found in this folder. The mock data is structured in such a way as it will imitate an actual Node/Mongo backend service if run using JSON-SERVER. Instructions on how to start json-server in this way are in the README in this folder.

/starterWithRouting
In some programmes, Routing for single page applications has not been covered. This is a starter scaffold project that has the routing elements already created and working within it and should be used as the starting point for the project.
