const express = require ("express");
const bodyParser = require("body-parser");
const router = express.Router();
const SignUp = require("../signup.model.js");

router.use(bodyParser.json());

router.route(`/`).get((req, res) => {

    SignUp.find((error, data) => {
        error ? res.status(404).send(`Not found`) : res.json(data);
    });
});

router.route('/').post((req, res) => {
    let signupData = new SignUp(req.body);
    signupData
        .save(signupData)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occured whilst adding the signup data"
            })
        })
});

module.exports = router;