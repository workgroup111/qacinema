const express = require(`express`);
const router = express.Router();
const bodyParser = require(`body-parser`);
const Film = require("../film.model.js");

router.use(bodyParser.json());

router.route(`/`).get((req, res) => {

    Film.find((error, films) => {
        error ? res.status(404).send(`Not found`) : res.json(films);
    });
});

router.route(`/`).post((req, res) => {

    let newFilm = new Film(req.body);
    newFilm
        .save(newFilm)
        .then(data=>{
            res.send(data);
        })
        .catch(err => {
        res.status(500).send({
            message: err.message || "Some error occured while creating the film"})
        })
});

module.exports = router;