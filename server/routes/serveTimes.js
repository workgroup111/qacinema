const express = require(`express`);
const router = express.Router();
const bodyParser = require(`body-parser`);
const openingTimes = require("../openingTimes.model.js");

router.use(bodyParser.json());

router.route(`/`).get((req, res) => {

    openingTimes.find((error, openingtimes) => {
        error ? res.status(404).send(`Not found`) : res.json(openingtimes);
    });
});

router.route(`/`).post((req, res) => {

    let newTime = new openingTimes(req.body);
    newTime
        .save(newTime)
        .then(data=>{
            res.send(data);
        })
        .catch(err => {
        res.status(500).send({
            message: err.message || "Some error occured while updating the times"})
        })
});

module.exports = router;