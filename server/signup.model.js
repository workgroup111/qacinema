const mongoose = require('mongoose');
const Schema = mongoose.Schema;

/*const Todo = new Schema({
    todoDescription: { type: String, required: true },
    todoDateCreated: { type: Date, default: Date.now, required: true },
    todoCompleted: { type: Boolean }
});*/

const SignSchema = new Schema({
    /* Title: { type: String, required: true },
     Firstname: { type: String, required: true },
     Lastname: { type: String, required: true },
     Email: { type: String, required: true },
     DOB: Date,
     PhoneNumber: Number,
     Gender: String, */
    Title: String,
    Firstname: String,
    Lastname: String,
    Email: String,
    DOB: Date,
    PhoneNumber: Number,
    Gender: String,
});

//module.exports = mongoose.model(`Todo`, Todo);

module.exports = mongoose.model(`SignSchema`, SignSchema);