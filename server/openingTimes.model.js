const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const openingTimes = new Schema({
    day: {type: String},
    opening: { type: String },
    close: { type: String }

});

module.exports = mongoose.model(`openingTimes`, openingTimes);