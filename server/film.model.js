const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Film = new Schema({
    title: { type: String, required: true },
    synopsis: { type: String, required: true },
    cast: { type: String },
    directors: { type: String },
    showingTimes: { type: String },
    realeaseDate: { type: Date },
    img: {type : String}

});

module.exports = mongoose.model(`Film`, Film);