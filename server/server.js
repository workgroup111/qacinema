const express = require('express');
const mongoose = require('mongoose');
const app = express();
const PORT = 4000;
const serveFilms = require('./routes/serveFilms');
const SignUp = require('./routes/signupfunc');
const bodyParser = require('body-parser');
var cors = require('cors');

const serveTimes = require('./routes/serveTimes');
app.use(cors());

app.use('/signup', SignUp);
app.use('/allfilms', serveFilms);
app.use('/openingTimes', serveTimes);


mongoose.connect("mongodb://mymongo/signup")
    .then(() => console.log("Successful connection to the database on port: 27017."))
    .catch(err => console.log("Error lol."));


const server = app.listen(PORT, () => {
    const SERVERHOST = server.address().address;
    const SERVERPORT = server.address().port;
    console.log("Server is running on port: ", PORT);
});

module.exports = server;